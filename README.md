# Jenkins| Setting up a CI/CD pipeline and deployment on AWS. 

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Context
Setting up a Jenkins continuous integration pipeline
1. Define a Dockerfile for the application [static-website](https://github.com/CarlinFongang/static-website)
2. Parameterize the listening port if the application needs to be hosted on Heroku
3. Implement Build, Test-acceptance, Release, Staging, Deploy, Test-functioning, Shared-library steps

## Requirements
Prepare 2 EC2 instances running Ubuntu 22.04 to support Jenkins for one and deployment of the application for the other.

## How We Will Proceed:
To effectively set up our CI/CD pipeline on Jenkins to automatically deploy the `static-website-example` project, we will:
1. Install and configure Jenkins
2. Set up a build phase where we will configure a Dockerfile and test it
3. Automate the build phase by integrating the webhook concept through installing and configuring the Github Integration | Build plugin
4. Implement the acceptance test phase to ensure that the produced Docker image is viable and returns a correct response | Acceptance Test
5. Once the image testing is done, we will then produce a release to ensure the backup and usability of the viable image produced in the build phase for phases requiring deployment in Staging/production | Release & Dockerhub
6. We will then set up a Staging phase that will allow deployment from the "main" branch to a dedicated Staging server
7. Once the Staging environment is deployed, we need to perform a test to ensure that the server responds/is online
8. We will set up deployment to the production environment only when there is a change on the main branch
9. Finally, we will set up a last phase to test the availability of the application in production.


## Jenkins Installation
1. SSH into the instance hosting Jenkins
   ```
   ssh -i key_pair.pem ubuntu@public_ip_address
   ```

2. Install Docker and Docker Compose | [labs: Docker installation](https://gitlab.com/CarlinFongang-Labs/docker-labs/docker-install)

3. Deployment of Jenkins | [labs: Jenkins installation](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab1-install)

4. Launch and initial configuration of Jenkins | [labs: Launching Jenkins](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab1-install#25-launch-jenkins)

## Manual Build and Test
To be performed on a machine dedicated to testing.
### 1. Dockerfile Definition and Manual Build
The code repository to build from is [here](https://github.com/CarlinFongang/static-website)

1. Dockerfile Definition
    ```Dockerfile
    ARG version="alpine"
    FROM nginx:$version
    LABEL maintainer="Carlinfg <fongangcarlin@gmail.com>"
    
    RUN apk update && apk add git
    WORKDIR /usr/share/nginx/html
    RUN rm -rf /usr/share/nginx/html/*
    RUN git clone https://github.com/CarlinFongang/static-website.git /usr/share/nginx/html
    ENV PORT 80
    EXPOSE $PORT
    
    CMD ["nginx", "-g", "daemon off;"]
    ```

    This Dockerfile defines an NGINX image using a specified version (default "alpine"). It updates packages and installs Git, then clones a GitHub repository containing a static website into the NGINX directory. It exposes port 80 and configures NGINX to run as a web server (daemon).

2. Build the image
   ```
   docker build -t static-website .
   ```
   >![alt text](img/image-1.png)
   *Docker image of the static-website application*

3. Launch the Docker container with the static-website image
   >![alt text](img/image-2.png)
   *Container running*

4. Viability test
   >![alt text](img/image-3.png)
   *The application returns a 200 response*

5. Usability test
   >![alt text](img/image-4.png)
   *Application accessible from the internet*

## Automation: Declaration of Build and Acceptance Test stages in the CI/CD pipeline in Jenkinsfile
### 0. Variable Declaration
```groovy
environment {
    IMAGE_NAME = "static-website"
    IMAGE_TAG = "latest"
    IP_ADDRESS = "34.200.227.139" //Jenkins server address
}
```

### 1. Definition of "Build" in Jenkinsfile
```groovy
stage('Build image') {
    agent any
    steps {
        script {
            sh 'docker build -t $IMAGE_NAME:$IMAGE_TAG .'  
        }
    }
}
```

### 2. "Container Launch" Stage
```groovy
stage('Run container based on built image') {
    agent any
    steps {
        script {
            sh '''
                docker run --name ${IMAGE_NAME}-test -d -p 80:80 $IMAGE_NAME:$IMAGE_TAG
                sleep 5
            '''
        }
    }
}
```

### 3. "Acceptance Test" Stage
```groovy
stage('Test image') {
    agent any
    steps {
        script {
            sh '''
                curl "http://$IP_ADDRESS"
            '''  
        }
    }
}
```

### 4. Project Configuration: Build and Test
1. Creation of the `static-website` pipeline
>![alt text](img/image-5.png)

2. Project Configuration
- Github Project repository
>![alt text](img/image-6.png)

````bash
https://github.com/CarlinFongang/static-website.git
````

- Declaration of the repository hosting the Jenkinsfile
>![alt text](img/image-8.png)

Once the pipeline is configured, save and launch the build.

3. Build and Test Result
>![alt text](img/image-9.png)
>![alt text](img/image-10.png)
*Effective build and test*

4. Cleaning the test environment once the test is successful
```groovy
stage('Clean test') {
    agent any
    steps {
        script {
            sh '''
                docker stop ${IMAGE_NAME}-test 
                docker rm ${IMAGE_NAME}-test
            '''
        }
    }
}
```
- Output
>![alt text](img/image-13.png)

- Console output
>![alt text](img/image-14.png)

### 5. Webhook Configuration
This plugin allows listening to changes made in the "static-website" project repository via the webhook service and automatically triggers the pipeline set up in Jenkins.
Note: Since the Jenkinsfile is located in a different repository, any changes to that repository do not trigger the pipeline.

1. Installation of Github Integration
Administer Jenkins > Plugins > Available Plugins > "Search for... : "Github Integration"
>![alt text](img/image.png)
*Selection and installation of Github Integration*

2. Activation of hook triggers for automatic pipeline launch upon repository update
Dashboard > static-website > Configure > Build Triggers
>![alt text](img/image-7.png)

3. Webhook Configuration
Github > static-website > settings > Webhooks > Add webhook
`http://ip_address_jenkins_server:8080/github-webhook/`
>![alt text](img/image-11.png)
*Webhook Configuration*

>![alt text](img/image-12.png)
*Active webhook status*

### 6. Release Stage
1. Creation of Dockerhub Access Token
The release backup will be done on Dockerhub. To perform this operation, we need to generate an Access Token, which will be passed as a variable to Jenkins and will allow us to authenticate during login and push of Docker images.
hub.docker.com > Account Settings > Security > Access Tokens > New access token
>![alt text](img/image-15.png)
>![alt text](img/image-20.png)

2. Creation of Jenkins credential for authentication to Dockhub
Dashboard > Manage Jenkins > Credentials > Global credentials > Add credentials

- Configuration of the Access Token
>![alt text](img/image-17.png)

3. Declaration of the environment variable in the stage
```groovy
 stage('Release image') {
    environment {
        DOCKERHUB_USER = credentials('DHUB_USER')
        DOCKERHUB_TOKEN = credentials('DHUB_TOKEN')                
    }
    agent any
    steps {
        script {
```
The declaration of the DOCKERHUB_TOKEN variable will allow its use as a password during authentication to Dockerhub for saving the image generated during the build.

4. Authentication and Image Push
```groovy
steps {
    script {
        sh 'docker tag $IMAGE_NAME:$IMAGE_TAG carlfg/$IMAGE_NAME:$IMAGE_TAG'
        sh 'docker tag $IMAGE_NAME:$IMAGE_TAG carlfg/$IMAGE_NAME:$GIT_COMMIT'
        sh 'echo $DOCKERHUB_TOKEN | docker login -u carlfg --password-stdin'
        sh 'docker push carlfg/$IMAGE_NAME:$IMAGE_TAG'
        sh 'docker push carlfg/$IMAGE_NAME:$GIT_COMMIT'  
        sh 'echo "Pushing the image to DockerHub..." '
    }
}
```
Script for authentication and image push to Dockerhub.

5. Execution Stage Result
>![alt text](img/image-21.png)
*Pipeline View*

>![alt text](img/image-22.png)
*Console Output*

>![alt text](img/image-23.png)
*Images pushed to Dockerhub*

### 7. Staging Stage
Deployment in the Staging environment allows us to validate the availability and proper functioning of the application before it is sent to production.

1. Create a new EC2 instance for the Staging environment
Declaration of a variable containing the IP address
```groovy
pipeline {
    environment {
        IMAGE_NAME = "static-website"
        IMAGE_TAG = "latest"
        IP_ADDRESS = "3.235.28.200"
        IP_Staging = "44.203.240.214"
```
Since our EC2 server is ephemeral, we can declare the IP address in clear text, but it is recommended to store it as a credential and then use it as an environment variable. It's a better security practice.

2. Definition of the "Deploy Staging" stage
```groovy
        stage('Deploy Staging') {   
            environment {
                EC2_SSH_KEY = credentials('DEVOPS')
                DOCKERHUB_USER = credentials('DHUB_USER')
                DOCKERHUB_TOKEN = credentials('DHUB_TOKEN')
            }
```
In this stage, we declare the environment necessary for using the key pair, token, and user for authentication to EC2 and Github for pulling the release.

```groovy
    agent any
    steps {
        script { 
            sh''' 
                ssh -o StrictHostKeyChecking=no -i $EC2_SSH_KEY ${USER_SERVER}@${IP_Staging} "
                docker login -u \$DOCKERHUB_USER -p \$DOCKERHUB_TOKEN
                docker pull \$DOCKERHUB_USER/\$IMAGE_NAME:\$IMAGE_TAG
                docker stop \$IMAGE_NAME || true
                docker rm \$IMAGE_NAME || true
                docker run -d --name \$IMAGE_NAME -p 80:80 \$DOCKERHUB_USER/\$IMAGE_NAME:\$IMAGE_TAG
                "
            '''
        }
    }
}
```
In this stage, we perform an SSH connection to the EC2 instance dedicated to deployment in Staging. Once authenticated, we log in to Dockerhub from this instance to retrieve the image of the static-webapp application and run a container with this image.

### 3. Jenkins Pipeline View
>![Pipeline Render](img/image-24.png)
*Pipeline View*

>![Console Output](img/image-25.png)
*Console Output*

### 4. Application Accessible on Staging Server
>![Staging Server Access](img/image-26.png)
*Access to the Staging Server*

### 8. Staging Test
```groovy
stage('Test Staging') {
    agent any
    steps {
        script {
            sh '''                        
                curl -I "http://$IP_Staging"
            '''  
        }
    }
}
```
Testing the Staging environment allows us to ensure the effective availability of the application once it is deployed.

1. Console Output
We have a successful 200 response in the console output when performing a curl on the review IP.
>![Review Console Output](img/image-30.png)
*Review Console Output*

### 8. Deployment to Production
The 'Deploy production' stage is conditioned to run only on the 'main' branch. Environment variables are defined for authentication and deployment configuration. The stage uses SSH to connect to the production server with the EC2 key pair. It then performs necessary Docker operations, including pulling the image, stopping and removing the existing container, and finally starting a new container with the updated image.

1. Jenkins Pipeline View
>![Pipeline View](imgimage-27.png)
*Pipeline View*

>![Console Output](img/image-28.png)
*Console Output*

2. Application Accessible on Production Server
>![Production Server Access](img/image-29.png)
*Access to the Production Server*

3. Testing the Availability of the Application in Production
>![Production Test Console Output](img/image-31.png)
*Production Test Console Output*