ARG version="alpine"
FROM nginx:$version
LABEL maintainer="Carlinfg <fongangcarlin@gmail.com>"

RUN apk update && apk add git
WORKDIR /usr/share/nginx/html
RUN rm -rf /usr/share/nginx/html/*
RUN git clone https://github.com/CarlinFongang/static-website.git /usr/share/nginx/html
ENV PORT 80
EXPOSE $PORT

CMD ["nginx", "-g", "daemon off;"]